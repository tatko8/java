package homework2;

import java.util.Random;
import java.util.Scanner;

public class Shooting {
    private static void createField(char[][] Array) {
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        for (int x = 0; x < Array.length; x++) {
            System.out.print(x + 1 + " | ");
            for (int y = 0; y < Array[x].length; y++) {
                System.out.print(Array[x][y] + " | ");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        Scanner Scanner = new Scanner(System.in);
        final int Row = 5;
        final int Column = 5;
        int RowX;
        int ColumnY;
        char[][] Array = new char[Row][Column];
        int targetRow = new Random().nextInt(Row);
        int targetColumn = new Random().nextInt(Column);
        boolean theEnd = false;


        for (int x = 0; x < Row; x++){
            for (int y = 0; y < Column; y++){
                if (x == targetRow && y == targetColumn){
                    Array[x][y] = 'x';
                } else {
                    Array[x][y] = '-';
                }
            }
        }

        System.out.print("All set. Get ready to rumble!\n");
        createField(Array);

        while(!theEnd){
            do {
                System.out.print("Enter a ROW from 1 to 5: ");
                RowX = Scanner.nextInt() - 1;
            } while ( RowX < 0 && RowX >= Row);

            do {
                System.out.print("Enter a COLUMN from 1 to 5: ");
                ColumnY = Scanner.nextInt() - 1;
            } while ( ColumnY < 0 && ColumnY >= Column);

            if(Array[RowX][ColumnY] == 'x'){
                Array[RowX][ColumnY] = 'x';
                theEnd = true;
                System.out.println("You have won!");
            } else if (Array[RowX][ColumnY] == '-'){
                Array[RowX][ColumnY] = '*';
                System.out.println("Miss, try again!");
            }
            createField(Array);
        }
    }
}
